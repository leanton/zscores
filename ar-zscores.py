import csv

from pandas import Series
from statsmodels.tsa.ar_model import AR


def export_separate_companies():
    comps = dict()
    with open('data/zscores.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            if row[2] != '#DIV/0!':
                k = int(row[1])
                v = tuple([row[0], float(row[2])])
                if k in comps:
                    arr = comps[k]
                    arr.append(v)
                    comps[k] = arr
                else:
                    comps[k] = [v]
    print(comps)
    for k, v in comps.items():
        myFile = open('data/zscore_' + str(k) + '.csv', 'w')
        with myFile:
            writer = csv.writer(myFile)
            writer.writerows(v)


def calculate_zscore():
    series = Series.from_csv('data/zscore_1191911.csv')
    # split dataset
    X = series.values
    train = X[0:len(X)]
    # train autoregression
    model = AR(train)
    model_fit = model.fit()
    window = model_fit.k_ar
    coef = model_fit.params
    # walk forward over time steps in test
    history = train[len(train) - window:]
    predictions = list()
    for t in range(1):
        length = len(history)
        lag = [history[i] for i in range(length - window, length)]
        yhat = coef[0]
        for d in range(window):
            yhat += coef[d + 1] * lag[window - d - 1]
        predictions.append(yhat)
        print('predicted=%f' % (yhat))


calculate_zscore()
# export_separate_companies()
